import unittest
from render.data_structures import Model
from render.triangle_iterator import SimpleIterator


class TestSimpleIteratorCase(unittest.TestCase):

    def setUp(self) -> None:
        class FakeModel(Model):
            def __init__(self):
                self._tris = ['A', 'B', 'C']

            def n_triangles(self) -> int:
                return 3

            def get_triangle(self, index: int):
                return self._tris[index]

        model = FakeModel()
        self.iterator = SimpleIterator(model)

    def test_iter(self):
        self.assertEqual(iter(self.iterator), self.iterator)

    def test_next(self):
        self.assertEqual(next(self.iterator), 'A')
        self.assertEqual(next(self.iterator), 'B')
        self.assertEqual(next(self.iterator), 'C')

        with self.assertRaises(StopIteration):
            next(self.iterator)


if __name__ == '__main__':
    unittest.main()
