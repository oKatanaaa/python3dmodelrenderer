import os
import unittest

import cv2
import numpy as np

from render.data_structures import Buffer


class TestBufferCase(unittest.TestCase):
    def setUp(self) -> None:
        self.test_image_source = cv2.imread('test_data/test_image.png')
        if self.test_image_source is None:
            self.test_image_source = cv2.imread('test/test_data/test_image.png')
        self.h, self.w = self.test_image_source.shape[:2]
        self.image = Buffer(self.h, self.w)

    def test_setitem(self):
        for xi in range(self.w):
            for yi in range(self.h):
                self.image[yi, xi] = self.test_image_source[yi, xi]
        self.assertEqual(np.sum(self.test_image_source - self.image.get_image()), 0)

    def test_set_pixel(self):
        for xi in range(self.w):
            for yi in range(self.h):
                self.image.set_pixel(xi, yi, self.test_image_source[yi, xi])
        self.assertEqual(np.sum(self.test_image_source - self.image.get_image()), 0)

    def test_getters(self):
        self.assertEqual(self.image.get_size(), self.test_image_source.shape[:2])
        self.test_set_pixel()
        color = self.image.get_pixel(1, 1)
        self.assertEqual(np.sum(color - self.test_image_source[1, 1]), 0)

    def test_writing(self):
        self.test_set_pixel()
        filename = 'test_image.png'
        self.image.write_to_file(filename)
        self.assertTrue(os.path.exists(filename))
        os.unlink(filename)


if __name__ == '__main__':
    unittest.main()
