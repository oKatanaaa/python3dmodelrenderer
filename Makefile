ifeq (, $(sh which python3))
  PYTHON = python3
  PIP = pip3
else
  PYTHON = python
  PIP = pip
endif

.PHONY = all help deps build test run
.DEFAULT_GOAL = help

help:
	@echo "----------HELP----------"
	@echo "To install all the dependencies 'make deps''"
	@echo "To build the project, type 'make build'"
	@echo "To test the project, type 'make setup'"
	@echo "To run the project type 'make run'"
	@echo "To do all (except deps step) just type 'make all'"
	@echo "------------------------"

deps:
	echo "Installing python dependencies"
	${PIP} install -r requirements.txt

build:
	echo "There is nothing to build, for now"

test:
	echo "Running all unit tests"
	${PYTHON} -m unittest discover test "Test_*.py"

run:
	echo "Starting the project"
	${PYTHON} run.py
